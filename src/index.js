import * as $ from 'jquery'
import './styles/index.scss'
import './babel'

$(document).ready(function() {
    var bgImage = new Image();
    var gifImage = new Image();

    var $window = $(window);

    function animation() {
        var window_height = $window.height();
        var window_top_position = $window.scrollTop();
        var $animation_elements = $('.element');
        var window_bottom_position = (window_top_position + window_height);

        $.each($animation_elements, function () {
            var $element = $(this);
            var element_height = $element.outerHeight();
            var element_top_position = $element.offset().top;
            var element_bottom_position = (element_top_position + element_height);

            if ((element_bottom_position >= window_top_position) && (element_top_position <= window_bottom_position)) {
                if (!$element.hasClass('animate')) {
                    $element.addClass('animate');
                }
            }
        });
    }

    bgImage.src = $('.absolute-bg__img').attr('src');

    bgImage.onload = function() {
        animation();

        $('body').addClass('loaded');

        gifImage.src = $('.main-wrapper__gif').attr('src');
        gifImage.onload = function() {
            $('.main-wrapper__gif-container').addClass('loaded')
        };

        $(window).on('scroll', function(event) {
            animation();
        });
    };
});