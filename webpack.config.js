const fs = require('fs')
// Встроенный модуль, который позволяет работаь с путями
const path = require('path')
// Плагин для работы с html или его препроцессорами
const HtmlWebpackPlugin = require('html-webpack-plugin')
// Плагин который очищает исходную папку куда складываеться код
const {CleanWebpackPlugin} = require('clean-webpack-plugin')
// Плагин для переноса файлов, копирование
const CopyWebpackPlugin = require('copy-webpack-plugin')
// Создает css для каждого js в котором он содежриться
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
// Оптимизирует и минифицирует js
const TerserWebpackPlugin = require('terser-webpack-plugin')
// Ищет все css и оптимизирует и минимизирует все файлы в процессе сборки
const OptimizeWebpackPlugin = require('optimize-css-assets-webpack-plugin')

// Переменная которая помогает определить режим текущей сборки
const isDev = process.env.NODE_ENV === 'development'
const isProd = !isDev

// Переменна оптимизации кода
const optimization = () => {
    const config = {
        // Ищет повторяющиеся конструкции кода, чтобы не грузить несколько раз одно и то же.
        // Вырезает, объединяет в специальные файлы ссылки, делает ссылки в местах выреза
        splitChunks: {
            chunks: 'all'
        }
    }

    // Если режим прод, применить дополнительные плагины при сборке
    if(isProd) {
        // Замена базовых оптимихаторов на свои
        config.minimizer = [
            new OptimizeWebpackPlugin(),
            new TerserWebpackPlugin
        ]
    }

    return config
}

// Универсальная перменная для именования файла, которая зависит от режима разработки
// Принимает в себя формат файла "ext" (js, css, html)
// const filename = ext => isDev ? `[name].${ext}` : `[name].[hash].${ext}`
const filename = ext => `[name].${ext}`
// Переменная включающая в себя обязательный для всех минификатор и css-loader
//принимающая в себя дополнительный extra плагин для конкретного лоадера (sass-loader, less-loader)
const cssLoaders = (extra) => {
    const loaders = [
            {
                loader: MiniCssExtractPlugin.loader,
                options: {
                    //Определение hot model reload, true зависит от режима сборки isDev
                    hmr: isDev,
                    reloadAll: true
                },
            },
            'css-loader'
    ]

    // Если есть допольнительный лоадер, то добавить в массив
    if(extra) {
        // в массив добавляем определенное значение экстра
        loaders.push(extra)
    }

    return loaders
}
// Общая функция которая возвращает стандартные настройки для сборки js (без дубликации)
// иначе можно добавить дополнительный пресет для частного случая
const babelOptions = (preset) => {
    const opts =  {
        //подключение пресета позволяющет юзать новейший js, без микроуправления
        // внтури пресет есть необходимые плагины для работы с js
        presets: [
            '@babel/preset-env'
        ],
        plugins: [
            '@babel/plugin-proposal-class-properties'
        ]
    }

    if (preset) {
        opts.presets.push(preset)
    }

    return opts
}

const PATHS = {
    src: path.join(__dirname, './src'),
    dist: path.join(__dirname, './dist')
}
const PAGES_DIR = `${PATHS.src}/pug/`
const PAGES = fs.readdirSync(PAGES_DIR).filter(fileName => fileName.endsWith('.pug'))

module.exports = {
    // Параметр в котором можно указать дирректорию
    // и больше не писать ее нигде
    context: path.resolve(__dirname, 'src'),
    // Параметр определяющий режим сборки
    mode: 'development',
    // Точка входа в строков формате,
    // может приниммать
    entry: {
        // Подключаем отдельную библиотеку-полифил для работы
        // с непереносимыми методами и объектами в старых браузерах
        main: ['@babel/polyfill', './index.js'],
        // Можно указать дополнительный подключаемый файл, сторонний полностью
    },
    // Указание куда будет помещен результат сборки
    output: {
        filename: filename('js'),
        path: path.resolve(__dirname, 'dist')
    },
    //
    resolve: {
        // Расширения файлов по-умолчанию (можно подключать и не писать .js, .json)
        extensions: ['.js', '.json'],
        // Позволяет указывать относительный путь в файлах для импорта
        alias: {
            '@models': path.resolve(__dirname, 'src/models'),
            '@': path.resolve(__dirname, 'src')
        }
    },
    // Параметр оптимизации проекта, вызов функции оптимизации
    optimization: optimization(),
    // Параметры дев сервера
    devServer: {
        port: 3000,
        // Горячая перезагрузка если режим development
        hot: isDev
    },
    // Ключ в конфигурации, где можно указать какие исходные карты нужны для раоты
    // При запуске в режиме development производить запуск соурс-мапа
    devtool: isDev ? 'source-map' : '',
    plugins: [
        new CleanWebpackPlugin(),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'src/assets'),
                to: path.resolve(__dirname, 'dist/assets/')
            }
        ]),
        new MiniCssExtractPlugin({
            filename: filename('css')
        }),
        ...PAGES.map(page => new HtmlWebpackPlugin ({
            template: `${PAGES_DIR}/${page}`,
            filename: `./${page.replace(/\.pug/,'.html')}`
        }))
    ],
    module: {
        rules: [
            {
                test: /\.pug$/,
                loader: 'pug-loader',
                options: {
                    // Указание собирать исходный html правильно, не в одну строку
                    pretty: true
                }
            },
            {
                test: /\.css$/,
                use: cssLoaders()
            },
            {
                test: /\.s[ac]ss$/,
                // Запуск cssLoader с дополнительным входным параметром
                // use - если массив, иначе для одного лоадера можно писать loaders
                use: cssLoaders('sass-loader')
            },
            {
                test: /\.(png|jpg|svg|gif)$/,
                 loader: 'file-loader'
            },
            {
                test: /\.(ttf|woff|woff2|eot|otf)$/,
                loader: 'file-loader'
            },
            {
                test: /\.js$/,
                // Исключить из поиска обработки js-файлов папку node_modules
                exclude: '/node_modules/',
                loader: {
                    loader: 'babel-loader',
                    options: babelOptions()
                }
            },
        ]
    }
}